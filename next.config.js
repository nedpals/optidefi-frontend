const { nextI18NextRewrites } = require('next-i18next/rewrites');
const localSubpaths = {};

module.exports = {
  rewrites: async () => nextI18NextRewrites(localSubpaths),
  publicRuntimeConfig: {
    localSubpaths,
  },
  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` module
    if (!isServer) {
      config.node = {
        fs: 'empty'
      }
    }

    return config
  }
}