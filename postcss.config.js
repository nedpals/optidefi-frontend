module.exports = {
    plugins: {
        "tailwindcss": {},
        "postcss-font-magician": {
            hosted: ["./src/assets/fonts"]
        }
    }
}