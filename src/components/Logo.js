export default function Logo({ className, loading = false }) {
  return (
    <svg className={`${className} ${loading ? 'loading' : ''}`} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 125 125">
      <defs/>
      <path className="p1" fillRule="evenodd" d="M125 62.5c0 34.518-27.982 62.5-62.5 62.5S0 97.018 0 62.5 27.982 0 62.5 0 125 27.982 125 62.5zm-62.5 54.784c13.97 0 26.718-5.229 36.395-13.836a55.09 55.09 0 008.289-9.245 54.543 54.543 0 008.183-17.287 54.82 54.82 0 001.917-14.416c0-30.256-24.528-54.784-54.784-54.784S7.716 32.244 7.716 62.5v.227a54.541 54.541 0 009.998 31.334 55.104 55.104 0 008.267 9.277c9.692 8.672 22.49 13.946 36.519 13.946z" clipRule="evenodd"/>
      <path className="p2" d="M115.367 76.916a54.82 54.82 0 001.914-13.834l-54.484-39.22-55.08 38.865c.019 4.777.65 9.41 1.818 13.826l53.19-37.53 52.642 37.893z"/>
      <path className="p3" d="M17.715 94.06a55.104 55.104 0 008.266 9.278l36.744-25.927 36.17 26.037a55.09 55.09 0 008.289-9.245L62.797 62.25l-45.082 31.81z"/>
      <style jsx>{`
      @keyframes beam2 {
        0% { fill: #fff; }
        50% { fill: #2CD65B; }
        100% { fill: #fff; }
      }
      svg path { fill: #2CD65B; }
      .loading .p1 {
        animation-name: beam2;
        animation-iteration-count: infinite;
        animation-direction: normal;
        animation-delay: 1.5s;
        animation-duration: 1.5s;
      }
      .loading .p2 {
        animation-name: beam2;
        animation-iteration-count: infinite;
        animation-direction: normal;
        animation-delay: 1s;
        animation-duration: 1.5s;
      }
      .loading .p3 {
        animation-name: beam2;
        animation-iteration-count: infinite;
        animation-direction: normal;
        animation-delay: 0.5s;
        animation-duration: 1.5s;
      }
      `}</style>
    </svg>
  );
}