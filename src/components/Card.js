import React from 'react';
import { generatePaddingCss } from './utils';

function Card({ bgColor = "#1C1C1C", children, padding = 4, className = null, onClick }) {
  const paddingCss = generatePaddingCss(padding);
  return (
    <div onClick={onClick} className={`card ${className}`}>
      {children}
      <style jsx>{`
        .card {
          @apply rounded;
          ${paddingCss}
          ${bgColor ? `background-color: ${bgColor};` : ''}
        }
      `}</style>
    </div>
  );
}

export default Card;