function generatePaddingClass(padding) {
  if (Array.isArray(padding)) {
    switch (padding.length) {
      case 1: return `p-${padding[0]}`;
      case 2: return `px-${padding[0]} py-${padding[1]}`;
      default: return '';
    }
  } else if (typeof padding == "object") {
    return `px-${padding['x']} py-${padding['y']}`;
  }
  return `p-${padding}`;
}

function computeRem(base) {
  return function (num) {
    return num/base;
  }
}

function generatePaddingCss(padding) {
  // p-4 = 1rem
  const compute = computeRem(4);
  if (Array.isArray(padding)) {
    switch (padding.length) {
      case 1: return `padding: ${compute(padding[0])}rem;`;
      case 2: return `padding: ${compute(padding[1])}rem ${compute(padding[0])}rem;`;
      default: return '';
    }
  } else if (typeof padding == "object") {
    const remX = compute(padding['x']) + ' rem';
    const remY = compute(padding['y']) + ' rem';
    return `padding: ${remY} ${remX};`;
  }
  return `padding: ${compute(padding)}rem;`;
}

export { generatePaddingClass, generatePaddingCss };