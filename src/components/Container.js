import React from 'react';

function Container({ children, className }) {
  return (
    <main className={className}>
      {children}
      <style jsx>{`
        main {
          @apply rounded-t;
          margin-left: auto;
          margin-right: auto;
          max-width: 71.25rem;
        }  
      `}</style>
    </main>
  );
}

export default Container;