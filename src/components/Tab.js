import React from 'react';

function TabGroup({ className, children }) {
  return (
    <ul className={`tabs ${className}`}>
      {children}
      <style jsx>{`
        .tabs {
          @apply flex flex-row;
        }
      `}</style>
    </ul>
  );
}

function Tab({ className, active = false, href, onClick, children }) {
  return (
    <li className={`${className}${active ? ' active' : ''}`}>
      <a onClick={onClick} href={href}>{children}</a>
      <style jsx>{`
        li {
          @apply text-center flex flex-1 font-medium;
        }
        li a {
          @apply w-full px-3 py-3;
          background-color: #151515;
        }
        li a:hover {
          @apply text-primary-500 bg-black;
        }
        li.active {
          @apply border-t-2 border-primary-500;
        }
        li.active a {
          @apply text-primary-500;
          background-color: #1C1C1C;
        }
        li.active a:hover {
          background-color: #151515;
        }
      `}</style>
    </li>
  );
}

export { TabGroup };
export default Tab;