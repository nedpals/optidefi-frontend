import React from 'react';

function Footer({ className }) {
  return (
    <>
    <p className={`footer ${className}`}>Copyright &copy; 2020 OptiDefi</p>
    <style jsx>{`
    .footer {
      @apply text-white py-6 text-lg;
    }
    `}</style>
    </>
  );
}

export default Footer;