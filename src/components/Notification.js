import React from 'react';
import Card from './Card';

function Notification({ title, icon, children, type = "primary", onClose }) {
  return (
    <Card padding="4" bgColor="" className={`notification ${type ? `is-${type}` : ``} flex flex-row items-center`}>
      {icon ? typeof icon == "string" ? (
        <span className={`material-icons text-${title ? '5xl' : '3xl'} pr-2`}>{icon}</span>
      ) : (
        <div className="text-2xl">
          {icon()}
        </div>
      ) : null}
      <div className="flex flex-col">
        {title ? (
          <h3 className="text-lg font-semibold">{title}</h3>
        ) : null}
        <p>{children}</p>
      </div>
      {onClose ? (
        <div className="ml-auto flex flex-row self-start">
          <button onClick={onClose} className={`material-icons ${type ? `text-${type}-800` : ``}`}>close</button>
        </div>
      ) : null}
      <style global jsx>{`
        .notification.is-primary {
          @apply bg-primary-500 border-primary-500 text-black;
        }
        .notification.is-danger {
          @apply bg-danger-500 border-danger-500;
        }
        .notification.is-warning {
          @apply bg-warning-500 border-warning-500;
        }
        .notification.is-info {
          @apply bg-info-500 border-info-500;
        }
        .notification.is-info-2 {
          @apply bg-info-2-500 border-info-2-500;
        }
      `}</style>
    </Card>
  );
}

export default Notification;