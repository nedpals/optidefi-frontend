import React, { useState } from 'react';
import Link from 'next/link';
import { withRouter } from 'next/router';
import Logo from '../components/Logo';
import ClientOnlyPortal from './Portal';
import { Icon } from './Placeholders';
import Button from './Button';
import Footer from './Footer';

// merge connect screen and login modal
function Modal({ onBgClick, router }) {
  const connectProvider = (e) => {
    // todo
    router.push("/");
  }

  return (
    <div onClick={onBgClick} className="backdrop">
      <div className="modal flex flex-col relative lg:flex-row mx-auto w-full rounded bg-layout text-white max-w-5xl mt-24 z-50">
        <button onClick={onBgClick} className="close-modal">
          <span className="material-icons">close</span> 
          <span className="lg:hidden">Close Modal</span>
        </button>
        <div className="w-full lg:w-1/2 rounded-l p-8 text-left flex flex-row lg:flex-col justify-between" style={{ 'backgroundColor': '#111111' }}>
          <div className="flex flex-col">
            <h2>Currently connected to</h2>
            <div className="flex flex-row items-center mt-8">
              <Icon sizeRem={4.4375} className="mr-4" />
              <div className="text-left">
                <h2 className="block text-2xl">Wallet XYZ</h2>
                <span className="block">0123456789abcd</span>
              </div>
            </div>
          </div>
          <div className="flex flex-row lg:flex-col">
            <p className="hidden lg:block mb-8">Tempus bibendum sit pellentesque orci nunc, massa proin in rutrum bibendum erat.</p>
            <Button as="button" className="my-8 lg:my-0" onClick={() => router.push("/connect")}>
              Disconnect Wallet
            </Button>
          </div>
        </div>
        <div className="w-full lg:w-1/2 flex flex-col rounded-r p-8 items-center  -mx-2 justify-center" style={{ 'backgroundColor': '#1C1C1C' }}>
          <h2 className="mb-4 text-2xl text-primary-500 font-bold text-left">Connect to other wallets</h2>
          <div className="w-full flex flex-row flex-wrap justify-center">
            {["Metamask", "Trezor", "Ledger", "Coinbase", "Portis"].map(provider => (
              <div key={provider} className="w-1/2 lg:w-full p-2 lg:px-0 flex items-center">
                <Button onClick={connectProvider} as="button" outline={true} type="primary" className="w-full text-xl text-center flex flex-row justify-center items-center">
                  <span className={`icon-${provider.toLowerCase()} mr-4`}></span>
                  <span>{provider}</span>
                </Button>
              </div>
            ))}
          </div>
          <Button as="button" outline={true} padding={[4,2]} className="w-full text-xl text-center my-3">
            Having trouble?
          </Button>
        </div>
      </div>
      <style jsx>{`
        .close-modal {
          @apply absolute flex flex-row;
          top: 3%;
          right: 3%;
        }
        .backdrop {
          @apply fixed inset-0 w-screen h-screen;
          background: rgba(0,0,0,0.4);
        }
      `}</style>
    </div>
  )
}

function MobileMenu({ active, router }) {
  const [walletModalState, setWalletModalState] = useState(false);
  const toggleWalletModalState = () => {
    setWalletModalState(!walletModalState);
  }

  return (
    <div className={`nav-menu ${active ? 'active' : ''}`} style={{display: "none"}}>
      <div className="main-menu">
        <button className="my-wallet-btn" onClick={toggleWalletModalState}>
          <span className="material-icons text-4xl mr-2 text-primary-500">account_balance_wallet</span>
          <div style={{ 'width': '15rem' }} className="text-left mr-auto">
            <span className="block text-xl font-semibold">My Wallet</span>
            <span className="font-medium block truncate">019201930903940394</span>
          </div>
          <span className="material-icons">chevron_right</span>
        </button>
        <ul className="nav-links">
          <li><Link href="/"><a>Home</a></Link></li>
          <li><Link href="/apy"><a>APY</a></Link></li>
          <li><Link href="/token"><a>Opti</a></Link></li>
          <li><Link href="/governance"><a>Gov</a></Link></li>
          <li><Link href="/about"><a>Info</a></Link></li>
          <li><Footer /></li>
        </ul>
      </div>
      {walletModalState ? (
        <ClientOnlyPortal selector="#portal">
          <Modal onBgClick={toggleWalletModalState} router={router} />
        </ClientOnlyPortal>
      ) : null}
      <style jsx>{`
        .nav-menu {
          @apply block fixed inset-x-0 text-white;
          top: 8%;
          bottom: 0%;
          background-color: #171717;
          max-height: 0;
          transition: max-height 0.3s ease-out;
        }

        .nav-menu.active {
          @apply px-6 py-4;
        }

        .nav-menu * {
          opacity: 0;
          transition: opacity 0.3s ease-out;
        }

        .nav-menu.active * {
          opacity: 1;
        }
        
        .nav-links {
          display: flex;
          flex-direction: column;
        }

        .nav-links li {
          @apply py-4;
        }

        .nav-links li a {
          @apply py-8 text-xl font-medium;
        }

        .nav-links li a:hover {
          @apply text-primary-500;
        }

        .my-wallet-menu {
          position: absolute;
          top: 1.3%;
          transform: translateX(100%);
        }

        .my-wallet-btn {
          @apply rounded py-4 px-4 w-full;
          display: flex;
          flex-direction: row;
          align-items: center;
          background: rgba(255, 255, 255, 0.1);
        }

        .my-wallet-btn:hover {
          background: rgba(255, 255, 255, 0.2);
        }

        @screen lg {
          .nav-menu {
            display: none !important;
          }
        }
      `}</style>
    </div>
  )
}

function Navbar({ className, router }) {
  const [walletModalState, setWalletModalState] = useState(false);
  const [mobileMenuState, setMenuState] = useState(false);
  const toggleWalletModalState = () => {
    setWalletModalState(!walletModalState);
  }

  const toggleMenu = (e) => {
    e.preventDefault();
    const menu = document.getElementById("portal").firstElementChild;
    const children = menu.children;
    if (!mobileMenuState) menu.style.display = "block";
    setTimeout(() => {
      for (let i = 0; i < children.length; i++) {
        children[i].style.opacity = mobileMenuState ? 0 : 1;
      }
    }, mobileMenuState ? 0 : 300);
    setTimeout(() => {
      menu.style.maxHeight = menu.style.maxHeight ? null : menu.scrollHeight + "px";
    }, mobileMenuState ? 200 : 0);
    setTimeout(() => {
      setMenuState(!mobileMenuState);
    }, 300);
    setTimeout(() => {
      if (mobileMenuState) menu.style.display = "none";
    }, 400);
  };

  return (
    <nav className={className}>
      <button className={`toggle-menu ${mobileMenuState ? 'open' : ''}`} onClick={toggleMenu}>
        <span className="line"></span>
        <span className="line"></span>
        <span className="line"></span>
      </button>
      <Logo className="logo" />
      <ul className="nav-links">
        <li><Link href="/"><a>Home</a></Link></li>
        <li><Link href="/apy"><a>APY</a></Link></li>
        <li><Link href="/token"><a>Opti</a></Link></li>
        <li><Link href="/governance"><a>Gov</a></Link></li>
        <li><Link href="/about"><a>Info</a></Link></li>
      </ul>
      <button className="my-wallet-btn" onClick={toggleWalletModalState}>
        <span className="material-icons text-3xl mr-2 text-primary-500">account_balance_wallet</span>
        <div style={{ 'width': '7.5rem' }} className="text-left">
          <span className="block font-semibold">My Wallet</span>
          <span className="text-xs font-medium block truncate">019201930903940394</span>
        </div>
        <span className="material-icons">chevron_right</span>
      </button>
      {walletModalState ? (
        <ClientOnlyPortal selector="#portal">
          <Modal onBgClick={toggleWalletModalState} router={router} />
        </ClientOnlyPortal>
      ) : null}
      <ClientOnlyPortal selector="#portal">
        <MobileMenu active={mobileMenuState} router={router} />
      </ClientOnlyPortal>
      <style jsx>{`
        nav {
          @apply pl-6 pr-16 py-4 shadow-md;
          display: flex;
          flex-direction: row;
          align-items: center;
          max-height: 4.375rem;
          height: 100%;
          color: #fff;
          background: #171717;
        }

        :global(.logo) {
          @apply mx-auto;
          width: auto;
          height: 100%;
        }

        .nav-links {
          @apply -mx-4;
          display: none;
          flex-direction: row;
        }

        .nav-links li a {
          @apply p-4 font-medium;
        }

        .nav-links li a:hover {
          @apply text-primary-500;
        }

        .toggle-menu {
          position: relative;
          width: 2rem;
          height: 1.9rem;
        }

        .toggle-menu .line {
          @apply bg-primary-500;
          display: block;
          position: absolute;
          height: 3px;
          width: 100%;
          border-radius: 9px;
          opacity: 1;
          left: 0;
          transform: rotate(0deg);
          transition: .25s ease-in-out;
        }

        .toggle-menu .line:nth-child(1) {
          top: 0%;
          transform-origin: left center;
        }

        .toggle-menu .line:nth-child(2) {
          top: 45%;
          transform-origin: left center;
        }

        .toggle-menu .line:nth-child(3) {
          top: 90%;
          transform-origin: left center;
        }

        .toggle-menu.open .line:nth-child(1) {
          transform: rotate(45deg);
          top: 0;
          width: 120%;
          left: 3px;
        }

        .toggle-menu.open .line:nth-child(2) {
          width: 0%;
          opacity: 0;
        }

        .toggle-menu.open .line:nth-child(3) {
          transform: rotate(-46deg);
          top: 90%;
          width: 120%;
          left: 3px;
        }

        .my-wallet-btn {
          @apply rounded py-1 px-2;
          display: none;
          flex-direction: row;
          align-items: center;
          margin-left: auto;
          background: rgba(255, 255, 255, 0.1);
        }

        .my-wallet-btn:hover {
          background: rgba(255, 255, 255, 0.2);
        }

        @screen lg {
          nav {
            @apply px-6 py-4;
          }
          .toggle-menu {
            display: none;
          }
          .nav-links,
          .my-wallet-btn {
            display: flex;
          }
          :global(.logo) {
            @apply ml-0 mr-10 pr-0;
          }
        }
      `}</style>
    </nav>
  )
}

export default withRouter(Navbar);