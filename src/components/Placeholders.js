import React from 'react';

function Icon({ sizeRem = 4, className = null }) {
  return (
    <div className={"placeholder " + className}>
      <style jsx>{`
        .placeholder {
          @apply bg-primary-500 rounded-full;
          height: ${sizeRem}rem;
          width: ${sizeRem}rem;
        }  
      `}</style>
    </div>
  )
}

export { Icon }