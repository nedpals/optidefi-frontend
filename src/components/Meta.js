import React from 'react';
import Head from 'next/head';

function Meta({ title }) {
  return (
    <Head>
      <title>{title ? title + ' - ' : ''}OptiDefi</title>
    </Head>
  )
}

export default Meta;