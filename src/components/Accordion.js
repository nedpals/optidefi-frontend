import React, { useState } from 'react';

function AccordionGroup({ children }) {
  return (
    <div className="accordions">
      {children}
      <style jsx>{`
        :global(.accordions .accordion:first-child) {
          @apply rounded-t;
        }
        :global(.accordions .accordion:last-child) {
          @apply rounded-b;
        }
      `}</style>
    </div>
  )
}


function Accordion({ title, children }) {
  const [isActive, setActive] = useState(false);
  const toggleAccordion = (e) => {
    e.preventDefault();
    const panel = e.currentTarget.nextElementSibling;
    const toggleIcon = e.currentTarget.firstChild;

    const rotate = (timestamp) => {
      panel.style.maxHeight = panel.style.maxHeight ? null : panel.scrollHeight + "px";
      if (!isActive) {
        toggleIcon.classList.add("clockwise");
        toggleIcon.classList.remove("counter-cc");
        toggleIcon.firstChild.style.opacity = 1;
        toggleIcon.lastChild.style.opacity = 0;
        panel.style.opacity = 1;
      } else {
        toggleIcon.classList.remove("clockwise");
        toggleIcon.classList.add("counter-cc");
        toggleIcon.firstChild.style.opacity = 0;
        toggleIcon.lastChild.style.opacity = 1;
        panel.style.opacity = 0;
      }

      setActive(!isActive);
      toggleIcon.style.transform = `rotate(${!isActive ? 90 : 0}deg)`;
    }

    window.requestAnimationFrame(rotate);
  }

  return (
    <div className={`accordion ${isActive ? 'active' : ''}`}>
      <div onClick={toggleAccordion} className="header">
        <div style={{transform: 'rotate(0deg)'}} className="toggle-icon origin-center">
          <span style={{transform: 'rotate(90deg)', opacity: 0}} className="origin-center material-icons">remove_circle</span>
          <span style={{opacity: 1}} className="material-icons">add_circle</span>
        </div>
        <h3>{title}</h3>
      </div>
      <div className="content">
        {children}
      </div>
      <style jsx>{`
        @keyframes clockwise {
          from { transform: rotate(0deg); }
          to { transform: rotate(90deg); }
        }
        @keyframes counter-clockwise {
          from { transform: rotate(90deg); }
          to { transform: rotate(0deg); }
        }
        .clockwise {
          animation: clockwise 0.3s ease-in;
        }
        .counter-cc {
          animation: counter-clockwise 0.3s ease-out;
        }
        .accordion {
          background-color: #1E1E1E;
        }
        .accordion .header {
          @apply select-none flex flex-row items-center py-4 pl-4 pr-8 cursor-pointer text-center;
        }
        .accordion .header .toggle-icon {
          @apply text-primary-500 relative;
          height: 1.71875rem;
          width: 1.71875rem;
        }
        .accordion .header .material-icons {
          @apply absolute top-0 left-0;
          transition: opacity 0.3s ease-in;
          font-size: 180%;
        }
        .accordion .header h3 {
          @apply mx-auto text-base font-medium;
          transition: color 0.2s linear;
        }
        .accordion.active .header h3 {
          @apply text-primary-500;
        }
        .accordion .content {
          @apply overflow-hidden max-w-2xl px-4 mx-auto;
          max-height: 0;
          opacity: 0;
          transition: max-height 0.4s ease-out, opacity 0.4s ease-out;
        }
        .accordion.active {
          @apply pb-8;
        }
        @screen lg {
          .accordion .header h3 {
            @apply text-xl;
          }
          .accordion .content {
            @apply px-0;
          }
        }
      `}</style>
    </div>
  );
}

export default Accordion;
export { AccordionGroup };