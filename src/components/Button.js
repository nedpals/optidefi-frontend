import React from 'react';
import { generatePaddingCss } from './utils';

function ButtonGroup({ className, children }) {
  return (
    <div className={`button-group ${className}`}>
      {children}
      <style jsx>{`
        .button-group {
          @apply flex flex-row rounded;
        }
        :global(.button-group .button) {
          @apply px-2 py-2 flex-1;
        }
        :global(.button-group .button:first-child) {
          @apply rounded-l rounded-r-none;
        }
        :global(.button-group .button:last-child) {
          @apply rounded-r rounded-l-none;
        }
        :global(.button-group.border .button:not(:first-child)) {
          @apply border-l rounded-none;
        }
      `}</style>
    </div>
  );
}

function Button({ as = "a", type = "primary", padding = 4, className, activeClassName, outline = false, active = false, children, ...otherProps }) {
  const paddingCss = generatePaddingCss(padding);
  const TagName = as;
  return (
    <>
      <TagName
        className={`button ${className} ${outline ? `outline` : ``} ${type ? `is-${type}` : ``} ${active ? `active ${activeClassName}` : ''}`} 
        {...otherProps}>
        {children}
      </TagName>
      <style jsx>{`
        .button {
          @apply rounded text-white;
          ${paddingCss}
        }
        .is-primary,
        .is-primary.active {
          @apply bg-primary-500 border-primary-500 text-black;
        }
        .is-primary.outline.active {
          @apply bg-primary-500 border-primary-500 text-white;
        }
        .is-primary:hover,
        .is-primary.outline:hover {
          @apply bg-primary-600 border-primary-600;
        }
        .is-primary.outline:hover {
          @apply text-black;
        }
        .is-danger,
        .is-danger.active,
        .is-danger.outline.active {
          @apply bg-danger-500 border-danger-500;
        }
        .is-danger:hover,
        .is-danger.outline:hover {
          @apply bg-danger-600 border-danger-600;
        }
        .is-warning,
        .is-warning.active,
        .is-warning.outline.active {
          @apply bg-warning-500 border-warning-500;
        }
        .is-warning:hover,
        .is-warning.outline:hover {
          @apply bg-warning-600 border-warning-600;
        }
        .is-info,
        .is-info.active,
        .is-info.outline.active {
          @apply bg-info-500 border-info-500;
        }
        .is-info:hover,
        .is-info.outline:hover {
          @apply bg-info-600 border-info-600;
        }
        .is-info-2,
        .is-info-2.active,
        .is-info-2.outline.active {
          @apply bg-info-2-500 border-info-2-500;
        }
        .is-info-2:hover,
        .is-info-2.outline:hover {
          @apply bg-info-2-600 border-info-2-600;
        }
        .outline {
          @apply border;
          background: transparent;
          color: #fff;
        }
      `}</style>
    </>
  );
}

export { ButtonGroup };
export default Button;