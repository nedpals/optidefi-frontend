import React from 'react';
import Container from './Container';
import Navbar from './Navbar';
import Meta from './Meta';
import Footer from './Footer';

function Layout({ title = 'Page', extendable = false, className = null, containerClassName, children, padLayout = false }) {
  return (
    <Container className={"layout-container " + containerClassName}>
      <Meta title={title} />
      <Navbar className="navbar" />
      <div className={"bg-layout content-area " + className + (padLayout ? " p-layout" : "")}>
        {children}
      </div>
      <Footer className="layout-footer" />
      <style jsx global>{`
        .layout-container {
          ${extendable ? 'min-height' : 'height'}: 90vh;
        }
        .bg-layout {
          @apply rounded-b;
          background-color: #111111;
        }
        .navbar {
          @apply rounded-t;
          ${extendable ? 'position: sticky; top: 0; left: 0; right: 0;' : ''}
        }
        .navbar .logo {
          width: 2.3rem;
          height: 2.375rem;
        }
        .p-layout {
          padding-left: 1.45rem;
          padding-right: 1.5rem;
        }
      `}</style>
      <style jsx>{`
        :global(.layout-footer) {
          @apply hidden;
        }
        .content-area {
          @apply overflow-y-auto text-white;
          max-height: 100%;
          height: 100%;
        }
        @screen lg {
          :global(.layout-container) {
            ${extendable ? 'min-height' : 'height'}: 88vh;
            margin-top: 2rem;
          }
          :global(.layout-footer) {
            @apply block;
          }
          :global(.p-layout) {
            padding-left:6.45rem;
            padding-right: 6.5rem;
          }
          .content-area {
            max-height: calc(100% - 4.375rem);
          }
        }
      `}</style>
    </Container>
  );
}

export default Layout;