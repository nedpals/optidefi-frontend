import React, { useState } from 'react';
import Layout from '../components/Layout';
import { Icon } from '../components/Placeholders';
import Button, { ButtonGroup } from '../components/Button';
import Card from '../components/Card';
import Tab, { TabGroup } from '../components/Tab';
import ClientOnlyPortal from '../components/Portal';

function ActionArea({ balance = 0.0, onExec, className = "", id="action-area", onClose, active = false }) {
  const [actionType, setActionType] = useState("earn");
  const [amount, setAmount] = useState(null);
  const toggleTab = (e) => {
    e.preventDefault();
    const tabId = e.target.href.substr(e.target.href.indexOf("#") + 1);
    setActionType(tabId);
  }

  return (
    <div id={id} className={`action-area ${className} ${active ? `translate-x-zero` : ``}`}>
      <Button onClick={onClose} padding="1" as="button" type="primary" outline={true} className="back-button">
        <span className="mt-1 -mr-2 material-icons">arrow_back_ios</span>
      </Button>
      <div className="action-header">
        <Icon sizeRem={6.75} className="mb-4" />
        <h2 className="text-3xl font-semibold">OPTI</h2>
        <p className="text-xl">OPTI Governance Token</p>
      </div>
      <div className="action-tabs">
        <TabGroup>
          <Tab onClick={toggleTab} active={actionType == "earn"} href="#earn">Earn</Tab>
          <Tab onClick={toggleTab} active={actionType == "claim"} href="#claim">Claim</Tab>
        </TabGroup>
        <div className="action-options">
          <div className="text-lg flex flex-row justify-between mb-6">
            <span className="font-regular">Balance</span>
            <span className="font-semibold">CRC{balance.toFixed(4)}</span>
          </div>
          <input type="text" value={amount} onChange={(e) => setAmount(parseFloat(e.target.value))} className="w-full mb-6" placeholder="Amount" />
          <ButtonGroup className="border border-primary-500 mb-6">
            {[0.25, 0.5, 0.75, 1.0].map(perc => (
              <Button padding={[4,3]} onClick={() => setAmount(balance*perc)} outline={true} as="button" className="hover:text-black" key={perc}>
                {perc.toFixed(2)*100}%
              </Button>
            ))}
          </ButtonGroup>
          <Button as="button" onClick={() => onExec(amount)} padding={[4,3]} className="font-semibold uppercase w-full">
            {actionType == "earn" ? "Earn" : "Claim"}
          </Button>
        </div>
      </div>
      <style jsx>{`
        @keyframes slide {
          from {
            transform: translateX(100%);
          }
          to {
            transform: translateX(0%);
          }
        }
        :global(.back-button) {
          @apply absolute;
          left: 5%;
          top: 3%;
          width: 20%;
        }
        .action-area.translate-x-zero {
          transform: translateX(0%);
        }
        .action-area {
          @apply flex-col text-white pt-8;
          height: inherit;
          width: 100%;
          background: #1C1C1C;
          transform: translateX(100%);
        }
        .action-area.active {
          transform: translateX(0%);
          animation: slide 0.2s ease-out;
        }
        .action-area.collapsing {
          transform: translateX(100%);
          animation: slide 0.2s ease-out reverse;
        }
        .action-area .action-header {
          @apply flex flex-col items-center pb-8;
        }
        .action-tabs {
          @apply flex flex-col flex-1;
        }
        .action-options {
          @apply h-full p-6 flex-1;
          background-color: #1C1C1C;
        }
        @screen lg {
          .action-area,
          .action-area.active,
          .action-area.collapsing {
            transform: translateX(0%);
          }
          .action-area {
            width: 40%;
          }
          :global(.back-button) {
            @apply hidden;
          }
        }
      `}</style>
    </div>
  )
}

export default function Home() {
  const [toggleArea, setToggleArea] = useState(false);
  const [balance, setBalance] = useState(100.0);

  const cryptos = [
    { code: 'USDT', name: 'USD Tether', rate: 0.0 },
    { code: 'USDC', name: 'USD Code', rate: 0.0 },
    { code: 'DAI', name: 'DAI Stablecoin', rate: 0.0 },
    { code: 'TUSD', name: 'TrueUSD', rate: 0.0 },
    { code: 'MUSD', name: 'mStable USD', rate: 0.0 },
    { code: 'USDO', name: 'USDO', rate: 0.0 }
  ];

  const toggleActionArea = (e) => {
    const parent = document.getElementById("action-area-mobile");
    parent.classList.toggle(!toggleArea ? "active" : "collapsing");
    setTimeout(() => {
      setToggleArea(!toggleArea);
    }, 200);
  }

  const deductBalance = (value) => {
    setBalance(balance - value);
  }

  return (
    <Layout title="">
      <div className="relative flex flex-row h-full overflow-x-hidden">
        <div className="crypto-list">
          {cryptos.map(c => (
            <Card onClick={toggleActionArea} padding={[4,3]} key={c.code} className="crypto-details">
              <Icon className="mr-4" />
              <div>
                <h2 className="block text-xl font-semibold">{c.code}</h2>
                <p className="block text-sm">{c.name}</p>
              </div>
              <div className="ml-auto text-right">
                <h2 className="block text-xl">{c.rate.toFixed(6)}%</h2>
                <p className="block text-sm">Interest Rate</p>
              </div>
            </Card>
          ))}
        </div>
        <ActionArea onExec={deductBalance} balance={balance} active={toggleArea} className="sticky inset-y-0 hidden lg:flex" />
        <ClientOnlyPortal selector="#portal">
          <ActionArea onExec={deductBalance} balance={balance} id="action-area-mobile" onClose={toggleActionArea} active={toggleArea} className="flex lg:hidden" />
        </ClientOnlyPortal>
      </div>
      <style global jsx>{`
      #action-area-mobile {
        @apply fixed inset-x-0 bottom-0;
        top: 9%;
      }
      .crypto-details {
        @apply cursor-pointer border text-white flex flex-row items-center my-2;
        background-color: #1C1C1C;
        border-color: #262626;
      }
      .crypto-details:hover {
        background-color: #292929;
      }
      `}</style>
      <style jsx>{`
      .crypto-list {
        @apply flex flex-col p-4;
        width: 100%;
        position: absolute;
      }
      .bg-action {
        background-color: #1C1C1C;
      }
      @screen lg {
        .crypto-list {
          @apply px-12 py-4;
          width: 60%;
          position: static;
        }
      }
      `}</style>
    </Layout>
  );
}
