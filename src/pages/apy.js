import React, { useRef } from 'react';
import Layout from '../components/Layout';
import Card from '../components/Card';
import { Bar } from 'react-chartjs-2';

function Apy() {
  const fundApyTableRef = useRef();
  const sampleData = {
    labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    datasets: [{
      label: '# of Votes',
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  }

  return (
    <Layout title="APY" className="py-12" padLayout={true} extendable={true}>
      <Card padding={6} className="mb-8">
        <div className="flex flex-col md:flex-row">
          <div className="flex-1 flex flex-col items-center text-center pb-4 md:pb-0">
            <h2 className="text-3xl font-semibold text-primary-500 mb-4">$123456.099990999</h2>
            <p className="text-xl">Fund Stablecoin Balance <br /><small>(DAI + USDC + USDT)</small></p>
          </div>
          <div className="flex-1 flex flex-col items-center text-center">
            <h2 className="text-5xl font-bold text-primary-500 mb-4">8.33%</h2>
            <p className="text-xl">APY</p>
          </div>
        </div>
        <span className="text-center block text-primary-500 mt-6">As of MM/DD/YY HH:MM:SSpm.</span>
      </Card>
      <Card padding={6} className="mb-8">
        <h2 className="text-xl">Fund APY Comparison Chart</h2>
        <Bar ref={fundApyTableRef} data={sampleData} />
      </Card>
      <Card padding={6} className="mb-12">
        <h2 className="text-xl">Fund Return Comparison Chart</h2>
        <Bar ref={fundApyTableRef} data={sampleData} />
      </Card>
    </Layout>
  );
}

export default Apy;