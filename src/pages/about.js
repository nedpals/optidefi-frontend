import React from 'react';
import Layout from '../components/Layout';
import { Icon } from '../components/Placeholders';
import { withTranslation } from '../i18n';

function Info() {
  return (
    <Layout title="About" extendable={true} className="py-12">
      <div className="flex flex-col items-center text-center max-w-xl mx-auto pb-12">
        <h1 className="font-bold text-5xl mb-4">About</h1>
        <p className="text-xl">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam purus elementum nunc, auctor vulputate faucibus mauris id neque. Cursus pharetra, quam proin quis nisl non. Quis convallis magna suscipit nisl.</p>
      </div>
      <div className="team-section p-layout">
        <div className="flex flex-col items-center text-center max-w-xl mx-auto pb-12">
          <h2 className="font-bold text-5xl mb-4">Team</h2>
          <p className="text-xl">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div className="list">
          {[...Array(9).keys()].map(k => (
            <div key={k} className="member">
              <Icon className="avatar mb-4" sizeRem={9.375} />
              <div className="details">
                <h3>Juan dela Cruz</h3>
                <p>Position Name</p>
              </div>
              <div className="social-links">
                <Icon className="mx-2" sizeRem={2.4375} />
                <Icon className="mx-2" sizeRem={2.4375} />
                <Icon className="mx-2" sizeRem={2.4375} />
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="flex flex-col items-center text-center p-layout py-12">
        <h2 className="font-bold text-5xl mb-4">Connect With Us</h2>
        <div className="social-links flex flex-col lg:flex-row mt-8">
          {[...Array(3).keys()].map(k => (
            <div className="flex flex-row items-center p-4">
              <Icon className="mr-4" sizeRem={3.3125} />
              <span className="text-2xl">@optidefi</span>
            </div>
          ))}
        </div>
      </div>
      <style jsx>{`
      .team-section {
        @apply py-12;
        background-color: #1C1C1C;
      }  
      .team-section .list {
        @apply flex flex-row flex-wrap justify-center;
      }
      .team-section .list .member {
        @apply w-1/2 flex flex-col items-center py-8;
      }
      .team-section .list .member .avatar {
        background-color: #fff !important;
      }
      .team-section .list .member .details {
        @apply mb-4 text-center;
      }
      .team-section .list .member .details h3 {
        @apply text-2xl font-semibold;
      }
      .team-section .list .member .social-links {
        @apply flex flex-row;
      }
      @screen lg {
        .team-section .list .member {
          @apply w-1/3 flex flex-col items-center py-8;
        }
      }
      `}</style>
    </Layout>
  );
}

export default Info;