import React from 'react';
// import Button from '../components/Button';
import Container from '../components/Container';
import Meta from '../components/Meta';
import Button from '../components/Button';
import { withRouter } from 'next/router';
import Footer from '../components/Footer';

function Connect({ router }) {
  const connectProvider = (e) => {
    // todo
    router.push("/");
  }

  return (
    <>
    <Container className="layout-container flex flex-col lg:flex-row mx-auto w-full bg-black text-white rounded">
      <Meta />
      <div className="w-full lg:w-1/2 rounded-l p-8 text-center flex flex-col items-center justify-center" style={{ 'backgroundColor': '#111111' }}>
        <img src="optidefi-logo.svg" alt=""/>
        <h1 className="text-5xl mb-4">Welcome to OptiDefi</h1>
        <p className="text-md">Amet sed laoreet morbi dolor mattis nec. Vulputate velit amet, at nulla gravida in senectus. Amet in pellentesque mauris sit auctor eget ullamcorper. Quam nunc sed vulputate a venenatis purus, tincidunt lorem. Pellentesque libero quam pulvinar dictum quam in amet, non lobortis. Cras aenean libero euismod.</p>
      </div>
      <div className="w-full lg:w-1/2 flex flex-col rounded-r py-16 lg:py-24 px-16 items-center justify-center" style={{ 'backgroundColor': '#1C1C1C' }}>
        <h2 className="mb-4 text-2xl text-primary-500 font-bold text-center text-left">Connect your wallet to get started.</h2>
        <div className="w-full flex flex-row flex-wrap justify-center">
          {["Metamask", "Trezor", "Ledger", "Coinbase", "Portis"].map(provider => (
            <div key={provider} className="w-1/2 lg:w-full p-2 lg:px-0 flex items-center">
              <Button onClick={connectProvider} as="button" outline={true} type="primary" className="w-full text-xl text-center flex flex-row justify-center items-center">
                <span className={`icon-${provider.toLowerCase()} mr-4`}></span>
                <span>{provider}</span>
              </Button>
            </div>
          ))}
        </div>
        <Button as="button" outline={true} padding={[4,2]} className="w-full text-xl text-center my-3">
          Having trouble?
        </Button>     
      </div>
    </Container>
    <Footer />
    <style global jsx>{`
      :global(.footer) {
        margin-left: auto;
        margin-right: auto;
        max-width: 71.25rem;
      }
      @screen lg {
        .layout-container {
          margin-top: 4rem;
        }
      }
    `}</style>
    </>
  );
}

export default withRouter(Connect);