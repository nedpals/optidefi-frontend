import React from 'react';
import Layout from '../components/Layout';
import Card from '../components/Card';
import Button from '../components/Button';
import Notification from '../components/Notification';

function Proposal({ padding = 8, className, data, children }) {
  return (
    <Card padding={padding} className={`proposal-card ${className}`}>
      <div className="w-full lg:w-2/3 lg:pr-8 flex flex-col items-center lg:items-start">
        {data.tags ? (
          <div className="block mb-4">
            {data.tags.map(tag => (
              <span className="mr-2 inline-block pill bg-primary-500 text-black px-4 py-1 rounded-full">
                {tag}
              </span>
            ))}
          </div>
        ) : null}
        <h2 className="title">{data.title}</h2>
        <p className="description text-center lg:text-left">{data.description}</p>
        <span className="text-primary-500">Passed on MM/DD/YY HH:MM:SS. Executed on MM/DD/YY HH:MM:SS</span>
      </div>
      <div className="w-full lg:w-1/3 pt-8 lg:pt-0 sm:px-24 lg:px-0 flex flex-col items-center justify-center">
        {children}
      </div>
      <style jsx>{`
        .title {
          @apply text-2xl font-bold mb-4 text-center;
        }
        .description {
          @apply text-lg mb-6 text-center;
        }
        :global(.proposal-card) {
          @apply flex flex-col mt-8;
        }
        @screen lg {
          :global(.proposal-card) {
            @apply flex flex-row mt-8;
          }
          .description,
          .title {
            @apply text-left;
          }
        }
      `}</style>
    </Card>
  );
}

function Governance() {
  return (
    <Layout title="Governance" padLayout={true} extendable={true} className="py-12">
      <Notification icon="info" type="info-2" title="Test">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vel interdum mauris sem elit purus.
      </Notification>
      <Proposal
        className="important"
        data={{
          tags: ["Lorem ipsum"],
          title: "Lorem ipsum dolor sit amet, consectetur.",
          description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Risus nunc rhoncus pretium elementum. Imperdiet."
        }} 
        padding={[8, 4]}>
        <Button as="button" type="danger" className="uppercase mb-4 w-full">Vote for No Support</Button>
        <span>1234567890CRC in Support</span>
      </Proposal>
      <div className="proposal-list">
        <h2 className="title">Finished Proposals</h2>
        {[...Array(2).keys()].map(k => (
          <Proposal
            key={k}
            data={{
              title: "Lorem ipsum dolor sit amet, consectetur.",
              description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Risus nunc rhoncus pretium elementum. Imperdiet."
            }}>
            <Button as="button" outline={true} padding={[8,4]} className="w-full uppercase mb-4">
              See Details
            </Button>
          </Proposal>
        ))}
      </div>
      <div className="proposal-list is-ongoing">
        <h2 className="title">Ongoing Proposals</h2>
        {[...Array(2).keys()].map(k => (
          <Proposal
            key={k}
            data={{
              title: "Lorem ipsum dolor sit amet, consectetur.",
              description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Risus nunc rhoncus pretium elementum. Imperdiet."
            }}>
            <Button as="button" className="w-full uppercase mb-4">
              Vote on Proposal
            </Button>
          </Proposal>
        ))}
      </div>
      <style jsx>{`
        .proposal-list {
          @apply pb-8 pt-12;
        }
        .proposal-list .title {
          @apply text-2xl;
        }
        :global(.card.important) {
          @apply border border-primary-500;
        }
        :global(.card.important .title) {
          @apply text-4xl font-bold;
        }
        :gloabl(.card.important .description) {
          @apply text-lg mt-4 mb-12;
        }
      `}</style>
    </Layout>
  )
}

export default Governance;