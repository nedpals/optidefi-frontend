import React from 'react';
import Layout from '../components/Layout';
import { Icon } from '../components/Placeholders';
import Card from '../components/Card';
import Button from '../components/Button';
import Accordion, { AccordionGroup } from '../components/Accordion';

function Token() {
  return (
    <Layout title="Token" extendable={true} padLayout={true} className="pt-12 pb-32">
      <div className="flex flex-col items-center text-center max-w-2xl mx-auto pb-12">
        <Icon sizeRem={8.875} className="mb-8" />
        <h1 className="font-semibold text-5xl mb-4">OPTI Token</h1>
        <p className="text-xl">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam purus elementum nunc, auctor vulputate faucibus mauris id neque. Cursus pharetra, quam proin quis nisl non. Quis convallis magna suscipit nisl.</p>
        <div className="flex flex-row mt-8">
          <Button as="button" outline={true} padding={[8,2]} className="mr-2">View Whitepaper</Button>
          <Button as="button" padding={[8,2]} className="ml-2">Add to Wallet XYZ</Button>
        </div>
      </div>
      <Card padding={8} className="token-stats mb-8">
        <div className="stat">
          <h2>#50</h2>
          <p>Rank</p>
        </div>
        <div className="stat">
          <h2>$1.63</h2>
          <span>(-0.6%)</span>
          <p>Current Price</p>
        </div>
        <div className="stat">
          <h2>$50M</h2>
          <p>Market Cap</p>
        </div>
        <div className="stat">
          <h2>$10M</h2>
          <p>Volume</p>
        </div>
      </Card>
      <Card padding={[8, 6]} className="mb-12">
        <h3 className="text-center block">Listed on these Exchanges</h3>
        <div className="flex flex-col md:flex-row -mx-4 my-6 items-center">
          <div className="w-2/3 md:w-auto md:flex-1 mx-4 bg-primary-500 h-12 mb-4 md:mb-0"></div>
          <div className="w-2/3 md:w-auto md:flex-1 mx-4 bg-primary-500 h-12 mb-4 md:mb-0"></div>
          <div className="w-2/3 md:w-auto md:flex-1 mx-4 bg-primary-500 h-12 mb-4 md:mb-0"></div>
          <div className="w-2/3 md:w-auto md:flex-1 mx-4 bg-primary-500 h-12 mb-4 md:mb-0"></div>
        </div>
        <h3 className="text-center block">+24 MORE</h3>
      </Card>
      <div>
        <h2 className="text-center font-semibold text-2xl mb-8">Frequently Asked Questions</h2>
        <AccordionGroup>
          {[...Array(3).keys()].map(k => (
            <Accordion key={k} title="Ante imperdiet pulvinar leo arcu lobortis?">
              <p>Iaculis feugiat non duis accumsan dolor ac aenean. Turpis elementum ullamcorper in sit sit. Nunc pharetra diam interdum fermentum fringilla dignissim. Viverra nibh arcu etiam tempus consequat. Diam felis, tristique facilisi non cursus adipiscing. Eget scelerisque porta elementum ut sagittis, eget. At platea sed est nibh netus accumsan ut.
  Massa elit viverra egestas luctus elit aliquet aliquam eget aliquet. Enim ipsum in vestibulum sollicitudin in. Viverra bibendum proin etiam augue. Molestie metus.</p>
            </Accordion>
          ))}
        </AccordionGroup>
      </div>
      <style global jsx>{`
        .token-stats {
          @apply border border-primary-500 bg-transparent flex flex-col;
        }
        .token-stats .stat {
          @apply flex-1 text-center flex flex-col justify-between pb-4;
        }
        .token-stats .stat h2 {
          @apply text-5xl text-primary-500 font-semibold;
        }
        .token-stats .stat span {
          @apply text-xl text-danger-500 mb-2;
        }
        .token-stats .stat p {
          @apply text-2xl;
        }
        @screen lg {
          .token-stats {
            @apply flex-row;
          }
          .token-stats .stat {
            @apply pb-0;
          }
        }
      `}</style>
      <style jsx>{`
        :global(.button),
        :global(.button.outline:hover) {
          @apply rounded bg-primary-500 text-black border-primary-500 border;
        }
        :global(.button.outline) {
          @apply bg-transparent text-primary-500;
        }
      `}</style>
    </Layout>
  )
}

export default Token;