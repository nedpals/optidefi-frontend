const { colors: { black, white, transparent } } = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [
    './src/pages/**/*.js',
    './src/components/**/*.js'
  ],
  theme: {
    colors: {
      black,
      white,
      transparent,
      'primary': {
        100: '#EAFBEF',
        200: '#CAF5D6',
        300: '#ABEFBD',
        400: '#6BE28C',
        500: '#2CD65B',
        600: '#28C152',
        700: '#1A8037',
        800: '#146029',
        900: '#0D401B',
      },
      'success': {
        100: '#F1FBEA',
        200: '#DBF5CB',
        300: '#C5EFAB',
        400: '#9AE26D',
        500: '#6FD62E',
        600: '#64C129',
        700: '#43801C',
        800: '#326015',
        900: '#21400E',
      },
      'danger': {
        100: '#FBEAED',
        200: '#F5CBD3',
        300: '#EFABB9',
        400: '#E26C84',
        500: '#D62D4F',
        600: '#C12947',
        700: '#801B2F',
        800: '#601424',
        900: '#400E18',
      },
      'warning': {
        100: '#FBF8EA',
        200: '#F5ECCB',
        300: '#EFE1AB',
        400: '#E2CB6D',
        500: '#D6B42E',
        600: '#C1A229',
        700: '#806C1C',
        800: '#605115',
        900: '#40360E',
      },
      'info': {
        100: '#EAF6FB',
        200: '#CBE9F5',
        300: '#ABDBEF',
        400: '#6DC1E2',
        500: '#2EA6D6',
        600: '#2995C1',
        700: '#1C6480',
        800: '#154B60',
        900: '#0E3240',
      },
      'info-2': {
        100: '#FBEFEA',
        200: '#F5D7CB',
        300: '#EFBFAB',
        400: '#E28E6D',
        500: '#D65E2E',
        600: '#C15529',
        700: '#80381C',
        800: '#602A15',
        900: '#401C0E',
      },
    },
    extend: {
      fontFamily: {
        'sans': [
          '"Exo"',
          'system-font',
          'sans-serif'
        ],
        'icon': [
          '"Material Icons"'
        ]
      },
    },
  },
  variants: {},
  plugins: [],
}
